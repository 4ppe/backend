from recipe.models import User, Recipe, Ingredient, Category
from rest_framework import serializers

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['google_id', 'favorites', 'shopping_list']

class RecipeSerializer(serializers.HyperlinkedModelSerializer):
    ingredients = serializers.StringRelatedField(many = True, read_only=True)
    class Meta:
        model = Recipe
        fields = ['id','name', 'description', 'ingredients','photo']

class IngredientSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Ingredient
        fields = ['id','name']


class ChildrenCategoriesList(serializers.RelatedField):
    def to_representation(self,value):  
        return {
            "id":   value.id,
            "name": value.name
            }


class CategorySerializer(serializers.HyperlinkedModelSerializer):
    children = ChildrenCategoriesList(many = True, read_only=True)
    class Meta:
        model = Category
        fields = ['id', 'name', 'children']
