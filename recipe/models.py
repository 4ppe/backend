from django.db import models


class Category(models.Model):
    name = models.CharField(max_length = 128)
    parent =  models.ForeignKey('self', on_delete = models.CASCADE, null = True, blank = True, related_name = "children")
    def __str__(self):
        return self.name

class Ingredient(models.Model):
    name = models.CharField(max_length = 128)
    def __str__(self):
        return self.name
 
class Recipe(models.Model):
    name = models.CharField(max_length = 128)
    description = models.TextField()
    category = models.ForeignKey(Category, on_delete = models.CASCADE)
    ingredients = models.ManyToManyField(Ingredient, related_name="recipe_ingredients")
    photo = models.CharField(max_length = 128, default = 'YOK')
    def __str__(self):
        return self.name

class User(models.Model):
    google_id = models.CharField(max_length = 128)
    favorites = models.ManyToManyField(Recipe, related_name="user_recipes")
    shopping_list = models.ManyToManyField(Ingredient, related_name="shopping_list")
    def __str__(self):
        return self.google_id
